import Vue from 'vue'
import Router from 'vue-router'
import login from '@/components/login'
import signUp from '@/components/signUp'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: login
    },
    {
      path: '/signup',
      name: 'SignUp',
      component: signUp
    }
  ]
})
